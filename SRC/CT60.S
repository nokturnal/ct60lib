;CT60.s
;for PureC
;Pawel Goralski'2004
;Comments, constructive criticism, bug reports send to: 
; e-mail: nokturnal@nokturnal.pl  

;Export labels
XDEF ct60_ReadCoreTemperature 	/* CT60  XBIOS function 0xc60a */
XDEF ct60_RWparameter 			/* CT60  XBIOS function 0xc60b */
XDEF ct60_cache 				/* CT60 XBIOS function 0xc60c */
XDEF ct60_FlushCache 			/* CT60 XBIOS function 0xc60d */
XDEF ct60_CacheCtrl 			/* CacheCtrl() XBIOS function 160 */

/* ************ Implementation *************** */
/* *************************** */
ct60_ReadCoreTemperature:
		move.w	d0,-(sp)
		move.w	#$c60a,-(sp)
		trap	#14
		addq.l	#4,sp
		RTS
/* *************************** */
ct60_RWparameter:
		move.l	d2,-(sp)	
		move.l	d1,-(sp)
		move.w	d0,-(sp)
		move.w	#$c60b,-(sp)
		trap	#14
		lea.l	12(sp),sp	
		RTS
/* *************************** */
ct60_cache:
		move.w	d0,-(sp)
		move.w	#$c60c,-(sp)
		trap	#14
		addq.l	#4,sp
		RTS
/* *************************** */
ct60_FlushCache:
		move.w	#$c60d,-(sp)
		trap	#14
		addq.l	#2,sp
		RTS
/* *************************** */
ct60_CacheCtrl:
		move.w	d1,-(sp)
		move.w	d0,-(sp)
		move.w	#160,-(sp)
		trap	#14
		addq.l	#6,sp
		RTS
;*********************************************************