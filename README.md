# CT60lib

CT60lib is a library for Pure C compiler and Atari Falcon030 computers with CT60/CT63 accelerator. Library is high level interface which enables access to extended functions of CT60/63 accelerator and mc68060 processor.