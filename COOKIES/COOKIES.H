#ifndef __COOKIES_H__
#define __COOKIES_H__

/* CT60LIB for Pure C */
/* Saulot/[NOKTURNAL]'2004 */
/* COOKIES.H */
/* Cookie Jar routines */

#include "ct60lib\PREREQ.H"
#include "COOKIES\C_DEF.H"

/** Cookie Jar base address */
#define COOKIE_JAR 0x5A0L

typedef struct
	{
	   /** name of the cookie*/
           LONG lCookie;
	   /** Cookie value */
           LONG lValue;
	} COOKIE;


/*CookiesJarPresent() <br>
* \return Returns true if CookieJar is present, false otherwise.
*/
bool CookiesJarPresent(void);
/*CookiesGetNumberOfCookies() <br>
* \return Returns number of Cookies.
*/
UWORD CookiesGetNumberOfCookies(void);
/*CookiesGetNumberOfCookies() <br>
* @param lCookie
* @param pCookie
* \return Returns true if lCookie (nearly all possible cookie values are in C_DEF.H)
* was found and pCookie points to the newfound COOKIE structure. If cookie with given
* lCookie value was not found the function returns false and pCookie should be NULL.
*/
bool CookiesGetCookie(LONG lCookie,COOKIE *pCookie);
#endif
